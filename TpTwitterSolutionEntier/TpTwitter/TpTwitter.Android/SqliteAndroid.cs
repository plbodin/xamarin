﻿using SQLite;
using TpTwitter.Droid;
using TpTwitter.Services;

[assembly: Xamarin.Forms.Dependency(typeof(SqliteAndroid))]
namespace TpTwitter.Droid
{
    public class SqliteAndroid : ISqliteConnection
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string appDataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);

            string path = System.IO.Path.Combine(appDataPath, dbName);

            SQLiteConnection connection = new SQLiteConnection(path);

            return connection;
        }
    }
}