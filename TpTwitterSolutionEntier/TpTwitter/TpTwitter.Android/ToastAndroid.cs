﻿using Android.App;
using Android.Widget;
using TpTwitter.Services;
using TpTwitter.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(ToastAndroid))]
namespace TpTwitter.Droid
{
    public class ToastAndroid : IShowMessage
    {
        public void ShowNotification(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short)
                 .Show();
        }
    }
}