﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace TpTwitter.Converter
{
    public class DateToDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime? date = value as DateTime?;

            if (date == null)
            {
                return string.Empty;
            }

            TimeSpan duration = DateTime.Now - date.Value;

            if (duration.Minutes == 0)
            {
                return $"{duration.Seconds}s";
            }

            if (duration.Hours == 0)
            {
                return $"{duration.Minutes}m";
            }

            if (duration.Days == 0)
            {
                return $"{duration.Hours}h {duration.Minutes}m";
            }

            return $"{duration.Days}j";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
