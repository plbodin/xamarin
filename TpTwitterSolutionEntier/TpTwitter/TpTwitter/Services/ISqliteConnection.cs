﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpTwitter.Services
{
    public interface ISqliteConnection
    {
        SQLite.SQLiteConnection GetConnection(string dbName);
    }
}
