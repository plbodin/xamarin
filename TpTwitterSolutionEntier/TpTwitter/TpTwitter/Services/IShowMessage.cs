﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpTwitter.Services
{
    public interface IShowMessage
    {
        void ShowNotification(string message);
    }
}
