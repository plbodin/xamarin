﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpTwitter.Services
{
    public class SqliteService
    {
        public SQLite.SQLiteConnection GetConnection()
        {
            return Xamarin.Forms.DependencyService
                  .Get<ISqliteConnection>()
                  .GetConnection("twitter.db");
        }

        // Filtre sur les types T ayant
        // un constructeur public sans paramètre
        public List<T> Get<T>() where T : new()
        {
            using (SQLite.SQLiteConnection cnx = this.GetConnection())
            {
                return cnx.Table<T>().ToList();
            }
        }

        public void Add<T>(T item)
        {
            using (SQLite.SQLiteConnection cnx = this.GetConnection())
            {
                cnx.Insert(item);
            }
        }
    }
}
