﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TpTwitter.Models;

namespace TpTwitter.Services
{
    public class TweetService
    {
        /// <summary>
        /// Déclaration d'un évenement via le "event EventHandler".
        /// On précise ici que l'évènement va recevoir en argument un Tweet.
        /// </summary>
        public event EventHandler<Tweet> OnNewTweet;

        #region Singleton
        private TweetService()
        {
        }
        private static TweetService _Instance;
        public static TweetService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new TweetService();
                }
                return _Instance;
            }
        }
        #endregion

        public void AddTweet(string pseudo, string message)
        {
            Tweet tweet = new Tweet
            {
                Message = message,
                Pseudo = pseudo,
                DateCreation = DateTime.Now
            };

            new SqliteService().Add(tweet);

            // Raise d'un événement
            this.OnNewTweet?.Invoke(this, tweet);
        }

        public List<Tweet> Tweets
        {
            get
            {
               return new SqliteService().Get<Tweet>();
            }
        }
    }
}
