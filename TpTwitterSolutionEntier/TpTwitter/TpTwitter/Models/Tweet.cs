﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace TpTwitter.Models
{
   public class Tweet
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? Id { get; set; }
        public string Pseudo { get; set; }
        public string Message { get; set; }
        public DateTime DateCreation { get; set; }
    }
}
