﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TpTwitter.MarkupExtensions
{
    [ContentProperty(nameof(Source))]
    public class ImageResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            // On vérifie que le paramètre Source est bien présent
            if (Source == null)
            {
                return null;
            }

            
            ImageSource imageSource = 
                ImageSource.FromResource(
                    $"TpTwitter.Resources.{Source}",
                    this.GetType().Assembly);

            return imageSource;
        }
    }
}
