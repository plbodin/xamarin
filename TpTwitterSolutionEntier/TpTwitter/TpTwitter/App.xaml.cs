﻿using System;
using TpTwitter.Models;
using TpTwitter.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TpTwitter
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            string platform = Device.RuntimePlatform;

            TargetIdiom idiom = Device.Idiom;

            this.InitializeDatabase();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // On lit le dictionnaire
            if (App.Current.Properties.TryGetValue("LastStart", out object lastStart))
            {
                // On est ici, ce n'est pas la première fois que l'application est lancée
            }

            // On écrit dans le dictionnaire
            App.Current.Properties["LastStart"] = DateTime.Now;           
        }

        private void InitializeDatabase()
        {
            // using permets de disposer un objet même si une
            // erreur surviens dans le code
            using (var connection = new SqliteService().GetConnection())
            {
                connection.CreateTable<Tweet>();
            }

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
