﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TpTwitter.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateTweetPage : ContentPage
    {
        public CreateTweetPage()
        {
            InitializeComponent();
        }

        private void CreateTweetViewModel_OnCreatedTweetSuccess(object sender, EventArgs e)
        {
            this.Navigation.PopAsync();
        }
    }
}