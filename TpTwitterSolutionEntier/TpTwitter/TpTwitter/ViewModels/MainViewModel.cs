﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TpTwitter.Models;
using TpTwitter.Services;
using Xamarin.Forms;

namespace TpTwitter.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public ObservableCollection<TweetViewModel> Tweets { get; set; }

        public MainViewModel()
        {
            List<TweetViewModel> tweets = TweetService.Instance
                                      .Tweets
                                      .OrderByDescending(t => t.DateCreation)
                                      .Select(t => new TweetViewModel(t))
                                      .ToList();

            this.Tweets = new ObservableCollection<TweetViewModel>(tweets);

            TweetService.Instance.OnNewTweet += Instance_OnNewTweet;
        }

        private void Instance_OnNewTweet(object sender, Tweet tweet)
        {
            this.Tweets.Insert(0, new TweetViewModel(tweet));
        }
    }
}
