﻿using System;
using System.Collections.Generic;
using System.Text;
using TpTwitter.Services;
using Xamarin.Forms;

namespace TpTwitter.ViewModels
{
    public class CreateTweetViewModel : TweetViewModel
    {
        public event EventHandler OnCreatedTweetSuccess;
        public Command CmdCreateTweet { get; set; }

        public CreateTweetViewModel()
        {
            this.CmdCreateTweet = new Command(this.CreateTweet);
        }

        private void CreateTweet()
        {
            TweetService.Instance.AddTweet(this.Pseudo, this.Message);

            // On "raise" l'évenement OnCreateTweetSuccess
            this.OnCreatedTweetSuccess?.Invoke(this, EventArgs.Empty);            
        }
    }
}
