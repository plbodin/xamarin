﻿using System;
using System.Collections.Generic;
using System.Text;
using TpTwitter.Models;

namespace TpTwitter.ViewModels
{
    public class TweetViewModel : BaseViewModel
    {
        private Tweet _tweet;
        private string _pseudo;
        private string _message;
        private DateTime _dateCreation;

        public string Pseudo
        {
            get => this._pseudo;
            set
            {
                this.Set(value, ref _pseudo);
            }
        }

        public string Message
        {
            get => this._message;
            set
            {
                this.Set(value, ref _message);
            }
        }

        public DateTime DateCreation
        {
            get => _dateCreation;
            set
            {
                Set(value, ref _dateCreation);
            }
        }

        public TweetViewModel() : this(new Tweet())
        {

        }

        public TweetViewModel(Tweet tweet)
        {
            if (tweet == null)
            {
                throw new ArgumentNullException(nameof(tweet));
            }

            this._tweet = tweet;
            this.Pseudo = tweet.Pseudo;
            this.Message = tweet.Message;
            this.DateCreation = tweet.DateCreation;
        }
    }
}
