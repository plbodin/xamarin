﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace TpTwitter.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, 
                new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Retourne true si la valeur a changé
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="field"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public virtual bool Set<T>(T value, ref T field, [CallerMemberName] string propertyName = null)
        {
            bool areEquals = EqualityComparer<T>.Default.Equals(value, field);

            if (areEquals)
            {
                return false;
            }

            field = value;
            this.RaisePropertyChanged(propertyName);

            return true;
        }
    }
}
