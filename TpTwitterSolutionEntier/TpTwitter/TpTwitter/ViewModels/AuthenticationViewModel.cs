﻿using System;
using System.Collections.Generic;
using System.Text;
using TpTwitter.Services;
using Xamarin.Forms;

namespace TpTwitter.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        private string _pwd;
        private string _login;
        private bool _isVisible = true;

        public string Password
        {
            get => _pwd;
            set
            {
                if (Set(value, ref _pwd))
                {
                    this.CmdLogin.ChangeCanExecute();
                }
            }
        }

        public string Login
        {
            get => _login;
            set
            {
                if (Set(value, ref _login))
                {
                    this.CmdLogin.ChangeCanExecute();
                }
            }
        }

        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                Set(value, ref _isVisible);
            }
        }

        public Command CmdLogin { get; set; }

        public AuthenticationViewModel()
        {
            this.CmdLogin = new Command(this.DoLogin, this.CanLogin);
        }

        private void DoLogin()
        {
            if (this.Password == "1234")
            {
                this.IsVisible = false;
            }
            else
            {
                DependencyService.Get<IShowMessage>()
                                 .ShowNotification("Identifiant(s) incorrect(s)");
            }
        }

        private bool CanLogin()
        {
            return !string.IsNullOrEmpty(this.Login) &&
                   !string.IsNullOrEmpty(this.Password);
        }
    }
}
