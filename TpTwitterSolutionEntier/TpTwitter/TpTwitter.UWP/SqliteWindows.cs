﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TpTwitter.Services;
using TpTwitter.UWP;

[assembly:Xamarin.Forms.Dependency(typeof(SqliteWindows))]
namespace TpTwitter.UWP
{
    public class SqliteWindows : ISqliteConnection
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string path = System.IO.Path.Combine(appDataPath, dbName);

            SQLiteConnection connection = new SQLiteConnection(path);

            return connection;
        }
    }
}
