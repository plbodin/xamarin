﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TpTwitter.Services;
using TpTwitter.UWP;
using Windows.UI.Popups;

[assembly:Xamarin.Forms.Dependency(typeof(AlertWindows))]
namespace TpTwitter.UWP
{
    public class AlertWindows : IShowMessage
    {
        public async void ShowNotification(string message)
        {
            MessageDialog dialog = new MessageDialog(message);

            IUICommand result = await dialog.ShowAsync();

            // Tout le code ici, va être exécuté une fois que le message
            // dialog va être fermé
            Debug.WriteLine("La modal viens d'être fermée.");
        }
    }
}
