﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLive.Models
{
    public class Film
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public string Titre { get; set; }
        public int Annee { get; set; }
    }
}
