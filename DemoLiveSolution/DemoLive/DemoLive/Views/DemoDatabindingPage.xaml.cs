﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoLive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DemoDatabindingPage : ContentPage
    {
        public string Toto { get; set; }

        public DemoDatabindingPage()
        {
            InitializeComponent();

            this.Toto = "toto";
            
            this.BindingContext = this;

            // ne marche pas, attendons MVVM pour rafraichir la vue 
            // de cette manière
            this.Toto = "titi";            

        }
    }
}