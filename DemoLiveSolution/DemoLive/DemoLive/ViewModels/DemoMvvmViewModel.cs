﻿using DemoLive.Models;
using DemoLive.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DemoLive.ViewModels
{
    public class DemoMvvmViewModel : BaseViewModel
    {
        private string _nomDuFilm;
        public string NomDuFilm
        {
            get => _nomDuFilm;
            set
            {
                if (this.Set(value, ref _nomDuFilm))
                {
                    // On préviens l'UI que le bouton est potentiellement
                    // plus clickable ou inversement.
                    // Cela aura pour effet d'exécuter la méthode CanExecute()
                    // de la command.
                    this.CmdCreateFilm.ChangeCanExecute();
                }
            }
        }

        public Command CmdCreateFilm { get; set; }

        public ObservableCollection<FilmViewModel> Films { get; set; }

        private DataService _DataService = new DataService();

        public DemoMvvmViewModel()
        {
            this.CmdCreateFilm = new Command(this.CreateFilm, this.CanCreate);

            List<Film> films = this._DataService.GetList<Film>();
            List<FilmViewModel> filmsViewModel = films.Select(f => new FilmViewModel(f))
                                                      .ToList();

            this.Films = new ObservableCollection<FilmViewModel>(filmsViewModel);
        }

        private void CreateFilm()
        {
            // Instanciation du modèle a partir des propriété du view model
            Film film = new Film
            {
                Titre = NomDuFilm
            };

            this._DataService.Add(film);

            this.Films.Add(new FilmViewModel(film));

            this.NomDuFilm = null;
        }

        private bool CanCreate()
        {
            return !string.IsNullOrEmpty(this.NomDuFilm);
        }
    }
}
