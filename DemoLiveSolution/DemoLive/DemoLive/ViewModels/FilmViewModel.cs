﻿using DemoLive.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLive.ViewModels
{
    public class FilmViewModel : BaseViewModel
    {
        private Film _Film;
        private string _Titre;
        private int _Annee;

        public string Titre
        {
            get => _Titre;
            set
            {
                Set(value, ref _Titre);
            }
        }

        public int Annee
        {
            get => _Annee;
            set
            {
                Set(value, ref _Annee);
            }
        }

        public FilmViewModel() : this(new Film())
        {
        }

        public FilmViewModel(Film film)
        {
            _Film = film;

            Titre = film.Titre;
            Annee = film.Annee;
        }
    }
}
