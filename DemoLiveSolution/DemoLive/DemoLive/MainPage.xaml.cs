﻿using DemoLive.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DemoLive
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //  Application.Current.MainPage = new Views.Page1();
            this.Navigation.PushAsync(new Views.Page1());
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new Views.TabbedPage1());
        }

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            IShowMessage service = DependencyService.Get<IShowMessage>();

            await service.ShowMessageAsync("Button clicked");

            Debug.WriteLine("Je suis exécuté après la fermeture de la popup (XF)");
        }
    }
}
