﻿using DemoLive.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoLive
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            string runtimePlatform = Device.RuntimePlatform;
            TargetIdiom idiom = Device.Idiom;

            new DataService().InitializeDatabase();

            this.MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
