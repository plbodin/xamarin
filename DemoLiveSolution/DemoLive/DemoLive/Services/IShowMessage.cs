﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DemoLive.Services
{
    public interface IShowMessage
    {
        Task ShowMessageAsync(string message);
    }
}
