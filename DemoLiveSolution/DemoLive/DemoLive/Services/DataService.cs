﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using SQLite;
using DemoLive.Models;

namespace DemoLive.Services
{
    public class DataService
    {
        public SQLiteConnection GetConnection()
        {
            ISqliteConnection connection = DependencyService.Get<ISqliteConnection>();

            return connection.GetConnection("demo.db");
        }

        public void InitializeDatabase()
        {
            using (SQLiteConnection cnx = this.GetConnection())
            {
                cnx.CreateTable<Film>();
            }
        }

        public List<T> GetList<T>() where T : new()
        {
            using (SQLiteConnection cnx = this.GetConnection())
            {
                return cnx.Table<T>().ToList();
            }
        }

        public void Add<T>(T item)
        {
            using (SQLiteConnection cnx = this.GetConnection())
            {
                cnx.Insert(item);
            }
        }
    }
}
