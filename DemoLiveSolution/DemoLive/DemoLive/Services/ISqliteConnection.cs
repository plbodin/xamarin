﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLive.Services
{
    public interface ISqliteConnection
    {
        SQLite.SQLiteConnection GetConnection(string dbName);
    }
}
