﻿using DemoLive.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;
using DemoLive.UWP.Implementations;

// Register dans le Xamarin dependency service de l'implementation
[assembly:Xamarin.Forms.Dependency(typeof(ShowMessageWindows))]
namespace DemoLive.UWP.Implementations
{
    public class ShowMessageWindows : IShowMessage
    {
        public async Task ShowMessageAsync(string message)
        {
            MessageDialog dialog = new MessageDialog(message);

            await dialog.ShowAsync();

            Debug.WriteLine("Ce code est exécuté après fermeture de la popup (UWP)");
        }
    }
}
