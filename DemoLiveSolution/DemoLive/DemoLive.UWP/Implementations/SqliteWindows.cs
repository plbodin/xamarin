﻿using DemoLive.Services;
using DemoLive.UWP.Implementations;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly:Xamarin.Forms.Dependency(typeof(SqliteWindows))]
namespace DemoLive.UWP.Implementations
{
    public class SqliteWindows : ISqliteConnection
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            path = Path.Combine(path, dbName);

            return new SQLiteConnection(path);
        }
    }
}
