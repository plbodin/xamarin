﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DemoLive.Droid.Implementations;
using DemoLive.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

[assembly:Xamarin.Forms.Dependency(typeof(SqliteAndroid))]
namespace DemoLive.Droid.Implementations
{
    public class SqliteAndroid : ISqliteConnection
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);

            path = Path.Combine(path, dbName);

            return new SQLiteConnection(path);
        }
    }
}