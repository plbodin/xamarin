﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DemoLive.Droid.Implementations;
using DemoLive.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly:Xamarin.Forms.Dependency(typeof(ShowMessageAndroid))]
namespace DemoLive.Droid.Implementations
{
    public class ShowMessageAndroid : IShowMessage
    {
        public Task ShowMessageAsync(string message)
        {          
            Toast t = Toast.MakeText(Application.Context, message, ToastLength.Short);

            t.Show();

            return Task.CompletedTask;
        }
    }
}