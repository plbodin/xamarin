﻿using System;
using System.Collections.Generic;
using System.Text;
using TpTwitter.Services;
using Xamarin.Forms;

namespace TpTwitter.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        private string _Identifier;
        public string Identifier
        {
            get => _Identifier;
            set
            {
                if (Set(value, ref _Identifier))
                {
                    CmdAuthenticate.ChangeCanExecute();
                }
            }
        }

        private string _Password;
        public string Password
        {
            get => _Password;
            set
            {
                if (Set(value, ref _Password))
                {
                    this.CmdAuthenticate.ChangeCanExecute();
                }
            }
        }

        private bool _SaveIdentifiers;
        public bool SaveIdentifiers
        {
            get => _SaveIdentifiers;
            set
            {
                this.Set(value, ref _SaveIdentifiers);
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return UserService.Instance.IsAuthenticated;
            }
        }

        public Command CmdAuthenticate { get; set; }
        public Command CmdLogout { get; set; }

        public AuthenticationViewModel()
        {
            this.CmdAuthenticate = new Command(this.Authenticate, this.CanAuthenticate);
            this.CmdLogout = new Command(this.Logout);
        }

        private void Logout()
        {
            UserService.Instance.Logout();
        }

        private void Authenticate()
        {
            bool success = UserService.Instance.Authenticate(
                this.Identifier,
                this.Password,
                this.SaveIdentifiers
                );

            this.RaisePropertyChanged(nameof(IsAuthenticated));

            if (!success)
            {
                App.Current.MainPage.DisplayAlert(
                    "Authentification",
                    "Identifiant(s) incorrect(s).",
                    "Fermer");
            }
        }

        private bool CanAuthenticate()
        {
            return !string.IsNullOrEmpty(Identifier) &&
                   !string.IsNullOrEmpty(Password);
        }
    }
}
