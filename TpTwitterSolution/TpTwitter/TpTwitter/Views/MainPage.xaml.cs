﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TpTwitter
{
    public partial class MainPage : ContentPage
    {
        private const string BgColorKey = "BgColor";
        public bool ShowSettings { get; set; }
        public string BgColor { get; set; } = "#58B0F0";

        public MainPage()
        {
            InitializeComponent();

            // Si on a une couleur de stockée
            if (Application.Current.Properties.TryGetValue(
                BgColorKey,
                out object codeHexa))
            {
                this.BgColor = codeHexa.ToString();
            }

            this.colorPicker.BgColor = this.BgColor;

            this.BindingContext = this;
        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            this.ShowSettings = !this.ShowSettings;

            // Hack : On supprimer le contexte
            this.BindingContext = null;

            // Et on remets le contexte. => ça permets
            // de faire croire a Xamarin que le contexte a changé
            // et de rafraichir les propriétés bindées
            this.BindingContext = this;
        }

        private void ColorPickerView_OnColorChanged(object sender, Color color)
        {
            this.BgColor = color.ToHex();
            Application.Current.Properties[MainPage.BgColorKey] = this.BgColor;

            this.BindingContext = null;
            this.BindingContext = this;
        }
    }
}
