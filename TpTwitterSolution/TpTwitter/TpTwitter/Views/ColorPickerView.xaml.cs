﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TpTwitter.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorPickerView : ContentView
    {      
        public event EventHandler<Color> OnColorChanged;

        public string BgColor
        {           
            set
            {
                string hexa = value;
                Color color = Color.FromHex(hexa);

                this.sliderR.Value = color.R;
                this.sliderG.Value = color.G;
                this.sliderB.Value = color.B;
            }
        }

        public ColorPickerView()
        {
            InitializeComponent();
        }

        private void sliderR_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Color color = Color.FromRgb(
                this.sliderR.Value,
                this.sliderG.Value,
                this.sliderB.Value);

            this.BgColor = color.ToHex();
            this.OnColorChanged?.Invoke(this, color);
        }
    }
}