﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpTwitter.Services
{
    public class UserService
    {
        private const string UserKey = "UserKey";

        private string _currentUserIdentifier;
        public bool IsAuthenticated
        {
            get => !string.IsNullOrEmpty(_currentUserIdentifier);
        }

        #region Singleton Pattern
        private static UserService _Instance;
        public static UserService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UserService();
                }

                return _Instance;
            }
        }

        private UserService()
        {
            if (App.Current.Properties.TryGetValue(UserKey, out object identifier))
            {
                _currentUserIdentifier = identifier.ToString();
            }
        }
        #endregion

        public bool Authenticate(string identifier, string password, bool saveIdentifiers)
        {
            if (identifier != "jacques" || password != "1234")
            {
                // On sors de la fonction si les identifiants sont incorrects
                return false;
            }

            this._currentUserIdentifier = identifier;

            if (saveIdentifiers)
            {
                // Sauvegarde physique sur le client de l'identifiant
                // de la personne connectée
                App.Current.Properties[UserKey] = identifier;
            }

            return true;
        }

        public void Logout()
        {
            App.Current.Properties.Remove(UserKey);
            this._currentUserIdentifier = null;
        }
    }
}
